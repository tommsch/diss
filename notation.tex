% !TeX encoding = UTF-8 Unicode
% !TeX root = diss.tex
% !TeX spellcheck = en_GB
\section{Definitions and Notation}\label{sec_notation}
%This section gathers all definitions and symbols we need from the beginning of the thesis up to the end.
The following notation is used throughout the thesis.

\begin{itemize}[align=left, leftmargin=0pt, labelindent=\parindent, 
    listparindent=\parindent, labelwidth=0pt, itemindent=!]
\item%
\glsadd{symb:CC}\glsadd{symb:RR}\glsadd{symb:RRplus}\glsadd{symb:QQ}\glsadd{symb:ZZ}\glsadd{symb:NN0}\glsadd{symb:NN}%
%\glsadd{symb:CCx}
We denote
by $\CC$ the \emph{complex numbers},
%by $\CCx$ the \emph{complex numbers without zero}, 
by $\RR$ the \emph{real numbers}, 
by $\RR_+$ the \emph{positive real numbers including zero}, 
by $\QQ$ the \emph{rational numbers}, 
by $\ZZ$ the \emph{integers}, 
by $\NN$ the \emph{positive integers without zero} and
by $\NN_0$ the \emph{positive integers including zero}.
By $\emptyset$ we denote the empty set.
For $z\in\CC$, we denote its real part by $\Re(z)$ and its imaginary part by $\Im(z)$.

\item%
\glsadd{symb:subseteq}%
\glsadd{symb:emptyset}\glsadd{symb:hashtag}%
Given sets $A$, $B$,  we write $A\subsetneqq B$ if $A$ is a \emph{strict subset} of $B$ and
$A\subseteq B$ if either $A=B$ or $A\subsetneqq B$.
We write $A\nsubseteq B$ if $A$ is not a subset of $B$.
The \emph{set difference} of $A$ and $B$ is denoted by $A\setminus B=\set{a\in A : a\notin B}$.
For a finite set $A$, we denote the \emph{number of elements} in $A$ by $\# A$.

\glsadd{symb:lebesgue}\glsadd{symb:pmpm}\glsadd{symb:Apown}%
Given $A,B\subseteq\RR^s$.
We denote the \emph{Lebesgue measure} of $A$ by $\lebesgue{A}$, if it exists.
We say that $A$ and $B$ are \emph{essentially equal}, denoted by $A\simeq B$, 
if they only differ by a set of Lebesgue-measure zero. 
We define 
$A\pm B=\set{a\pm b : a\in A,\ b\in B}$ and 
$AB=\set{ab:a\in A,\ b\in B}$.
For $M\in\RR^{s\times s}$ we define 
$MA=\set{Ma : a\in A}$.
For a finite set of matrices $\mathcal{A}=\set{A_j\in\RR^{s\times s}:j=1,\ldots,J}$, $J\in\NN$, we denote with
$\mathcal{A}^n$, $n\in\NN$, all matrix products of length $n$ with matrices from $\mathcal{A}$.

\glsadd{symb:interior}\glsadd{symb:closure}\glsadd{symb:boundary}%
Let $X$ be a topological space and $A\subseteq X$. 
We denote by $A^\interior$ the \emph{interior} of $A$, 
by $\closure (A)$ the \emph{closure} of $A$ and 
by $\partial A$ the \emph{boundary} of $A$.

\item%
\glsadd{Multi index notation}%
\glsadd{symb:munu}\glsadd{symb:!}\glsadd{symb:abs}\glsadd{symb:zpowM}\glsadd{Length_index}%
Let $\mu=(\mu_1,\ldots,\mu_s)\in\NN_0^s$. 
An element of $\NN_0^s$ is called \emph{multi-index} and written as a row-vector within parentheses.
Given $\mu,\nu\in\NN_0^s$ multi-indices, we define 
the \emph{length} $\abs{\mu}=\mu_1 + \cdots + \mu_s$,
the \emph{sum} and the \emph{difference} $\mu\pm\nu=(\mu_1\pm\nu_1,\ldots,\mu_s\pm\nu_s)$ whenever the right hand side is defined, 
a \emph{partial ordering} $\nu\leq\mu \Leftrightarrow \nu_l\leq\mu_l$ for all $l\in\set{1,\ldots,s}$ and
the \emph{factorial} $\mu!=\mu_1!\cdots \mu_s!$.
If $\nu\leq\mu$, then we define the \emph{binomial coefficient} by
\begin{equation}\label{equ_multivariate_binom}
\binom{\mu}{\nu}=
\frac{\mu!}{\nu!(\mu-\nu)!}=
\prod_{l=1}^{s}\binom{\mu_l}{\nu_l}=
\binom{\mu_1}{\nu_1}\binom{\mu_2}{\nu_2}\cdots\binom{\mu_s}{\nu_s}.
\end{equation}
For $z\in\CC^s$, $M\in\ZZ^{s\times s}$, $\alpha\in\ZZ^s$, we define 
the \emph{power} $z^\alpha=z_1^{\alpha_1}\cdots z_s^{\alpha_s}$
and the \emph{matrix power} $z^M=(z^{M_1},\ldots,z^{M_s})$, where $M_l$ denotes \emph{the $l^{th}$ column} of $M$.

\item%
\glsadd{symb:YpowX}\glsadd{symb:ell}%
%\emph{Sequence spaces}:
	Let $X,Y$ be sets, $X$ countable. We denote by $Y^X$ or $\ell(X, Y)$ the set of \emph{sequences} of elements in $Y$ indexed by elements in $X$. 
    If $Y=\RR$, then we also write $\ell(X)$.
	An element of $Y^X$ is denoted by $(y_x)_{x\in X}$, $(y_x)_x$ or $\boldsymbol{y}$.
	%The notation $Y^X$ is borrowed from topology and set theory.
	
\glsadd{symb:supp}\glsadd{symb:normellp}%
	For a sequence $a\in\ell(X, Y)$ we denote the value at index $x\in X$ with $a(x)$ or $a_x$.
	The \emph{support} of a sequence $a\in\ell(X)$ is defined by
    $\supp a=\set{x\in X:a(x)\neq 0}$.
	With $\ell_0(X)$ we denote finitely supported sequences.
    With $\ell_p(\ZZ^s)$, $1\leq p <\infty$, 
    we denote sequences with finite $p$-Norm defined by 
    $\norm{c}^p_p=\norm{c}^p_{\ell_p}=\sum_{\alpha\in \ZZ^s} \norm{c(\alpha)}_p^p$, 
    where $\norm{c(\alpha)}_p$ is the p-vector norm of $c(\alpha)$ defined by
    $\norm{c(\alpha)}^p_p=\sum_{l=1}^{s}\abs{c(\alpha)_l}^p$. 
    With $\ell_\infty(\ZZ^s)$ we denote bounded sequences with finite supremum norm defined by
    $\norm{c}_\infty =\norm{c}_{\ell_\infty}= \sup_{\alpha\in\ZZ^s}\abs{c(\alpha)}$.
    
     \glsadd{glos:boldfont}\glsadd{symb:delta}%
    The \emph{bold number} in a sequence or matrix denotes the zero$^{th}$ entry. 
    We denote with $\delta\in\ell(\ZZ^s)$ the \emph{Kronecker delta}, $\delta=\tbmatrix{\mathbf{1}}$, i.e.\ $\delta(\alpha)=1$ for $\alpha=0$ and zero otherwise.    

\glsadd{symb:el}\glsadd{symb:I}\glsadd{symb:rho}
    We identify finite sequences with vectors, matrices and tensors, and thus
    we settle for the choice to write sequences in $\ell(\ZZ)$ as column-vectors.
    Given $A\in\RR^{s\times s}$, we denote
    by $A^T$ the \emph{transpose} of $A$,
    by $A^*$ the \emph{conjugate transpose} of $A$,
    by $\rho(A)$ the \emph{spectral radius} of $A$ and
    by $\det(A)$ the \emph{determinant} of $A$,
%   by $\rank(A)$ the \emph{rank} of $A$.
%   For invertible matrices we define $A^{-T}=(A^{-1})^T$.
    We denote with $I$ the \emph{identity matrix}
    and with $e_l$ the $l^{th}$ \emph{standard unit vector} of $\RR^s$.
    
    \glsadd{symb:RRpowItimesJ}\glsadd{Generalized matrix}%
    Given countable sets $I,J$ and $m_{i,j}\in\RR$ for $i\in I$, $j\in J$. 
    We say $(m_{i,j})_{i\in I,j\in J}\in\RR^{I\times J}$ is a \emph{generalized $I\times J$ matrix}~\citep*[Section~2.1]{CHM2004}.
    Generalized matrices may always be realized as ordinary matrices by choosing a specific ordering 
    for the sets $I$ and $J$.
   
    \glsadd{symb:bracket}%
    All sequences and matrices are written within square-brackets $[\vardot]$. 
    Closed and open intervals are denoted by $[\quad]$ and $(\quad)$, respectively.
    
\item%
%\glsadd{symb:esssup}%
%\glsadd{Essential supremum}%
%\glsadd{symb:Lp}%
%	For $1\leq p \leq \infty$, 
%	by $L_p(\RR^s)$ we denote the Banach space of all real-valued measurable functions 
%    $f$ on $\RR^s$ such that $\norm{f}_p<\infty$, where
%	$\norm{f}_p^p=\norm{f}_{L_p}^p=\int_{\RR^s} \abs{f(x)}^p\, dx$ for $1\leq p < \infty$ and 
%    $\norm{f}_\infty=\norm{f}_{L_\infty}=\esssup\abs{f}$.
%
%\glsadd{symb:dpartial}\glsadd{Differential operator}\glsadd{symb:Dmu}%	
%	The \emph{partial derivative} of a function $f$ with respect to the $l^{th}$ coordinate is denoted by $\partial_l f$.
%	For a multi-index $\mu\in\NN_0^s$ we define the \emph{differential operator}
%	 $D^\mu=\partial_1^{\mu_1}\cdots\partial_s^{\mu_s}$.

%\glsadd{symb:Wkp}%	
%	For $1\leq p \leq \infty$ and $k\in\NN_0$ we denote with $W^k_p$ 
%the Sobolev space of weakly differentiable functions such that $D^\mu f\in L_p(\RR^s)$ 
%for all multi-indices $\abs{\mu}\leq k$. 
%Equipped with the norm $\norm{f}_{W^k_p}=\sum_{\abs{\mu}\leq k} \norm{D^\mu f}_{p}$ 
%this becomes a Banach space.

%\glsadd{symb:Ck}%	
%	For $k\in\NN_0$, we denote by $C^k(\RR^s)$ the space of 
%    $k$-times continuously differentiable functions from $\RR^s$ to $\RR$ with the norm 
%$\norm{f}_{C^k}=\sum_{\substack{\mu\in\NN_0^s\\ \abs{\mu}\leq k}} \norm{D^\mu f}_{\infty}$.
\glsadd{symb:Calpha}%	    
For $\alpha\in(0,1]$ we denote by $C^{\alpha}(\RR^s)$ the Hölder continuous functions with exponent $\alpha$,
i.e.\ $f\in C^\alpha$ if there exists $C>0$ such that
$\abs{f(x)-f(y)}\leq C\norm{x-y}^\alpha$ for all $x,y\in\RR^s$.


\item%

\glsadd{symb:alphabeta}
%\glsadd{symb:g}%
%\glsadd{symb:a}
%\glsadd{symb:M}
%\glsadd{symb:D}\glsadd{symb:d}%
\glsadd{symb:s}%
\glsadd{symb:l}%
We mostly use the letters $\alpha$, $\beta$, $\gamma$ as indices and
the letters $f$, $g$, $\phi$, $\psi$ for functions.
We usually denote
by $c$ a \emph{sequence}, 
by $a$ a finitely supported  \emph{mask sequence}, 
by $M$ a \emph{dilation matrix},
%$D$ a \emph{digit set}, $d$ \emph{digits},
by $s$ the dimension,
by $l$ the running index of the dimension, and by
$i$ we denote either an index or the imaginary unit $i^2=-1$.
\emph{Index sequences} are usually denoted by~$\seqj$, $\seqj\in\set{1,\ldots,J}^\NN$,
and their elements are denoted by~$j_1$, $j_2$, etc., i.e.\ without using bold font.

\item%
\glsadd{symb:floor}\glsadd{symb:span}\glsadd{symb:innerproduct}%
The \emph{floor function}, 
taking $x\in\RR$ and returning the greatest integer less than or equal to $x$ is denoted by $\floor{x}$.
The \emph{linear span} (or \emph{linear hull}) of a set $V$ is denoted by $\spn V$. 
The \emph{inner product} of $x,y\in\RR^s$ is denoted by $(x,y)$.

\glsadd{Linear programming}\glsadd{symb:simeq}%\glsadd{symb:qedsymbol}
\glsadd{symb:examplesymbol}%
%The Latin abbreviation \emph{i.e.} stands for \emph{it est}, whose English meaning is \emph{that is}.
We abbreviate $\JSR$ for \emph{joint spectral radius}, 
$\RSR$ for \emph{restricted spectral radius} and
\emph{LP} for linear programming.
The shorthand notation $j=1,\ldots,J$ is used to denote $j\in\set{1,\ldots,J}$.
For $a,b\in\RR$ we write $a\simeq b$ if the values of $a$ and $b$ are approximately equal.
The symbol~$\qedsymbol$ closes proofs, the symbol~$\examplesymbol$ closes examples.
\end{itemize}

\vspace{2\baselineskip}
\begin{center}
Having established ourselves on this sound basis, it is our duty to see what inferences can~be~drawn~\citep*{Doyle1893}.
\end{center}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EOF %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   