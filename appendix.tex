% !TeX encoding = UTF-8 Unicode
% !TeX root = diss.tex
% !TeX spellcheck = en_GB

\chapter{Appendix}\label{sec_appendix}
Theorem~\ref{thm_minimalOmega} proofs Conjecture~\ref{conj_minimalOmega}.
\begin{theorem}\label{thm_minimalOmega}%
    If $\mathcal{S}^\NN$ is convergent, then there exists a unique, with respect to inclusion, 
    minimal set $\Omega\subseteq\ZZ^s$
    such that $\ell(\Omega)$ is $\mathcal{T}$ invariant.
\end{theorem}
\begin{proof}
Assume the contrary. Then there exist sets $\Omega_1,\Omega_2\subseteq\ZZ^s$, finite, such that
$\ell(\Omega_1),\ell(\Omega_2)$ are $\mathcal{T}$-invariant.
In particular, the set $\Omega=\Omega_1\cup\Omega_2$ is $\mathcal{T}$-invariant and the transition matrices
$T_{d,j,\Omega}$, $d\in D_j$, $j\in\set{1,\ldots,J}$, have the form
\begin{equation*}
T_{d,j,\Omega}=\tbmatrix{
T_{d,j,\Omega_1} &0\\0&T_{d,j,\Omega_2}
}.
\end{equation*}
Both matrices $T_{d,j,\Omega_1},T_{d,j,\Omega_2}$ have an eigenvalue 1.
Thus, $T_{d,j,\Omega}$ has an eigenvalue 1 whose corresponding eigenvector is not simple.
But, by the uniqueness of the basic limit functions, Proposition~\ref{thm_blf_properties}~\ref{thm_blf_properties_unique},
the eigenvector to the eigenvalue 1 of the transition matrices of convergent subdivision schemes is unique, which is a contradiction.
\end{proof}

\begin{lemma}
The problem~\ref{rem_coast_absco} can be rewritten to
\begin{align}\label{rem_coast_absco_2}
\norm{x}_{\operatorname{absco} V}^{-1} &=
\begin{cases}
\max t_0\in\RR &\\
\text{subject to }  &\sum_{v\in V} \Re(t_v)\Re(v)-\Im(t_v)\Im(v)=t_0\Re(x)\\
                    &\sum_{v\in V} \Re(t_v)\Im(v)+\Im(t_v)\Re(v)=t_0\Im(x)\quad\text{and}\\
                    &\sum_{v\in V} \sqrt{\Re(t_v)^2+\Im(t_v)^2}= 1
\end{cases}
\end{align}
\end{lemma}
\begin{proof}
First note that $t_0\geq0$ always.
Assume there exists a solution to the problem 
\begin{align}\label{eq_coast_absco_2}
\begin{cases}
 \sum_{v\in V} \Re(t_v)\Re(v)-\Im(t_v)\Im(v)=t_0\Re(x)\\
 \sum_{v\in V} \Re(t_v)\Im(v)+\Im(t_v)\Re(v)=t_0\Im(x)\\
 \sum_{v\in V} \sqrt{\Re(t_v)^2+\Im(t_v)^2}= \alpha
\end{cases}
\end{align}
for some $t_0\in\RR$, $\alpha<1$.
Then setting $\tilde{t}_v=\alpha^{-2} t_v$ one obtains
\begin{align*}
\begin{cases}
 \sum_{v\in V} \Re(\tilde{t}_v)\Re(v)-\Im(\tilde{t}_v)\Im(v)=\alpha^{-2} t_0\Re(x)\\
 \sum_{v\in V} \Re(\tilde{t}_v)\Im(v)+\Im(\tilde{t}_v)\Re(v)=\alpha^{-2} t_0\Im(x)\\
 \sum_{v\in V} \sqrt{\Re(t_v)^2+\Im(t_v)^2}= \alpha\alpha^{-2/2}=1 
\end{cases}
\end{align*}
and $\alpha^{-2}t_0>t_0$. Therefore,~\eqref{eq_coast_absco_2} is not a solution to~\eqref{rem_coast_absco}.
\end{proof}

Unfortunately,~\citep*[page 17]{GP2013} is wrong as the next example shows.
\begin{example}\label{ex_GP2013}
Let 
$V_1\equiv\begin{bmatrix}-2\\i\end{bmatrix}$,
$V_2\equiv\begin{bmatrix}i\\2\end{bmatrix}$,
$p\equiv\begin{bmatrix}i\\-1\end{bmatrix}$.
Thus,
$x_1=\begin{bmatrix}-2\\ \phantom{-}0\end{bmatrix}$,
$y_1=\begin{bmatrix} 0\\ 1\end{bmatrix}$,
$x_2=\begin{bmatrix} 0\\ 2\end{bmatrix}$,
$y_2=\begin{bmatrix} 1\\ 0\end{bmatrix}$ and
$x  =\begin{bmatrix} \phantom{-}0\\-1\end{bmatrix}$,
$y  =\begin{bmatrix} 1\\ 0\end{bmatrix}$.
Below is a plot of the ellipses and the convex hull. The norm of $p$ should be $\sqrt{5/2}$.

\begin{center}
\begin{tikzpicture}
    \draw [red]  (0,0) ellipse (2cm and 1cm);
    \node[red] at (0cm,2.5cm) {$V_2$};
    \draw [red]  (0,0) ellipse (1cm and 2cm);
    \node[red] at (2.5cm,0cm) {$V_1$};
    \node[red] at (2.2cm,1.2cm) {$co(\{V_1,V_2\})$};
    \draw [blue] (0,0) ellipse (1cm and 1cm);
    \node[blue] at (0.5cm,0.5cm) {$p$};

%    \draw [green] (0,0) ellipse (1.581138830084190 and 1.581138830084190);
    \draw [red]  ( 1.788854381999832, 0.447213595499958) -- ( 0.447213595499958, 1.788854381999832);
    \draw [red]  (-1.788854381999832, 0.447213595499958) -- (-0.447213595499958, 1.788854381999832);
    \draw [red]  ( 1.788854381999832,-0.447213595499958) -- ( 0.447213595499958,-1.788854381999832);
    \draw [red]  (-1.788854381999832,-0.447213595499958) -- (-0.447213595499958,-1.788854381999832);
\end{tikzpicture}
\end{center}

This yields the conic programming problem \emph{(CP)} with variables $t_0\geq0$, $t_i,u_i\in\RR$, $s_i\geq0$, $i\in\{1,2\}$,
\begin{gather*}
\begin{cases*}
t_0 \rightarrow \max\\
\sqrt{t_1^2+u_1^2}\leq s_1 \text{ and } \sqrt{t_2^2+u_2^2}\leq s_2\\
s_1+s_2\leq 1\\
t_0 x = t_1 x_1 - u_1 y_1 + t_2 x_2 - u_2 y_2\\
t_0 y = u_1 x_1 + t_1 y_1 + u_2 x_2 + t_2 y_2
\end{cases*}
=
%\begin{cases*}
%t_0 \rightarrow \max\\
%\sqrt{t_1^2+u_1^2}+\sqrt{t_2^2+u_2^2}\leq 1\\
%t_0 x = t_1 x_1 - u_1 y_1 + t_2 x_2 - u_2 y_2\\
%t_0 y = u_1 x_1 + t_1 y_1 + u_2 x_2 + t_2 y_2
%\end{cases*}
%\\=
\begin{cases}
t_0 \rightarrow \max\\
\sqrt{t_1^2+u_1^2}+\sqrt{t_2^2+u_2^2}\leq 1\\
\begin{aligned}
0    &= - 2t_1              -  u_2 \\
-t_0 &=        - u_1 + 2t_2        \\
t_0  &= - 2u_1       +  t_2        \\
0    &=        \phantom{-}t_1        + 2u_2
\end{aligned}
\end{cases}
\end{gather*}
Solving gives $t_1=0$, $t_2=-t_0$, $u_1=-t_0$, $u_2=0$.
Thus, the above system becomes
\begin{gather*}
\begin{cases}
t_0 \rightarrow \max\\
\sqrt{0^2+(-t_0)^2}+\sqrt{(-t_0)^2+0^2}\leq 1\\
\end{cases}
=
\begin{cases}
t_0 \rightarrow \max\\
2|t_0|\leq 1
\end{cases}.
\end{gather*}
Therefore, $t_0=\frac{1}{2}$.

Additionally, for the ellipse $q\equiv\begin{bmatrix}i\\1\end{bmatrix}$, 
which describes the same ellipse as $p$, the result of the CP is different. Namely, $t_0=\frac{3}{2}$.
\end{example}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EOF %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%