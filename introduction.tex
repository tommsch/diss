% !TeX encoding = UTF-8 Unicode
% !TeX root = diss.tex
% !TeX spellcheck = en_GB


\chapter{Introduction}%\label{chp_one}

Subdivision schemes are fast and robust methods for generating
smooth (hyper)-surfaces from a given set of control points, usually in dimensions two or three.
We consider the case, when 
a subdivision operator maps a discrete polygonal mesh of data points, locally isomorphic to $\ZZ^s$, 
to a finer mesh of data points in the same space, locally isomorphic to $M^{-1}\ZZ^s$, 
where $M\in\ZZ^{s\times s}$ is a matrix all of whose eigenvalues are greater than one in modulus.
The matrix $M$ is referred to as \emph{dilation matrix}.
The vertices of the finer mesh are computed by weighted local averages of the coarser mesh.
The weights are referred to as the \emph{mask}.
A subdivision scheme is an iterated application of subdivision operators on a polygonal mesh, 
making the mesh finer and finer and eventually, maybe, converging to a smooth limit curve or surface.

We give as an Example the very first subdivision scheme, invented by~\citet{Rahm1947} and popularized by~\citet{Chai1974}.

\begin{example}[\emph{Corner cutting algorithm}, \citep*{Rahm1947,Chai1974}]\label{ex_cornercutting}
%    \glsadd{Corner cutting}%
\YY{To Maria: Changed example}
    Consider the polygonal chain defined by the sequence of points $(c_\alpha)_{\alpha\in\ZZ}$, $c_\alpha\in\RR^2$, with
    \begin{equation*}
    \ldots, \quad
    c_0=\tbmatrix{0\\2},\quad
    c_1=\tbmatrix{2\\0}, \quad
    c_2=\tbmatrix{4\\2}, \quad
    c_3=\tbmatrix{2\\1}, \quad
    \ldots
    \end{equation*}
    plotted in Figure~\ref{fig_cornercutting}~(a).
    For every point $p_\alpha\in\RR^2$, $\alpha\in\ZZ$,
    we compute two new points $c_\alpha',c_\alpha''\in\RR^2$ as a linear combination of the two neighbouring points with weights $\frac{1}{4}$ and $\frac{3}{4}$, precisely,
    \begin{equation*}
    c_\alpha'=\frac{3}{4}c_\alpha+\frac{1}{4} c_{\alpha-1},\quad
    c_\alpha''=\frac{3}{4}c_\alpha+\frac{1}{4} c_{\alpha+1},\quad \alpha\in\ZZ.
    \end{equation*}
    We obtain the sequence $\tbmatrix{\ldots & c_0' &c_0'' &c_1' &c_1'' & \ldots}$
    plotted in Figure~\ref{fig_cornercutting}~(b),
    which are refined in the same manner again.
    In Figure~\ref{fig_cornercutting} we see the generated polygonal chain after one, two and three subdivision steps.
    
    If we define $a=\frac{1}{4}\tbmatrix{\mathbf{1}&3&3&1}\in\ell_0(\ZZ)$,
    then we can express one subdivision step by
    \begin{equation*}
        c \mapsto Sc=\sum_{\beta\in\ZZ} a(\vardot-2\beta) c(\beta),\quad c\in\ell(\ZZ,\RR^2).\qedhere
    \end{equation*}   
\end{example}
%For the following mathematical treatment we assume 
%that the sequences $c$ to be refined are in $\ell(\ZZ^s)$; instead of periodic as Example\ref{ex_cornercutting}. 

\begin{figure}
    \centering
    \includegraphics{./graphics/cornercutting/cornercutting_0}\hspace{1em}
    \includegraphics{./graphics/cornercutting/cornercutting_1}\hspace{1em}
    \includegraphics{./graphics/cornercutting/cornercutting_2}\hspace{1em}
    \includegraphics{./graphics/cornercutting/cornercutting_3}
    \caption[Sequence of points from Example~\ref{ex_cornercutting}]%
    {Sequence of points as generated in Example~\ref{ex_cornercutting}.}
    \label{fig_cornercutting}
\end{figure}

\glsadd{Stationary subdivision}
First subdivision schemes with level independent subdivision weights and 
uniform underlying mesh, so-called \emph{stationary} schemes like in Example~\ref{ex_cornercutting}, 
appeared in the 1960s and are related to the
wavelet and frame theory and found applications in signal processing and image compression.
Because subdivision proved to be fast, robust and easy to use,
the theory of subdivision influenced several applied
areas of mathematics and engineering and, in return, has been
influenced by applications.
Thus, a big variety of subdivision methods were invented.
We give a brief, far from exhaustive, overview of various types of subdivision schemes. 
For a more thorough survey about subdivision schemes, surfaces and methods see 
e.g.~\citep*{Sabin2005,PR2008,Cash2012} and the references therein.
\begin{itemize}[leftmargin=5mm]
    \item%  \glsadd{Non-uniform subdivision}%
    \emph{Non-uniform subdivision} schemes allow for different weights for the computation of each point of the sequence. 
    They are mostly needed for the generation of surfaces with meshes of non-uniform topology,
    for example surfaces with boundary~%
    \citep*{DGS1999,PR2008,WW2001}.
    %Note that, sometimes non-uniform schemes are called non-stationary and vice-versa.    
    
    \item% \glsadd{Non-stationary subdivision}\glsadd{Level dependent subdivision}%
    \emph{Non-stationary subdivision} (or \emph{level-dependent subdivision})  schemes 
    use different subdivision weights in each step of the iteration.
    They are used in isogeometric analysis and biological imaging
    by exploiting their ability to generate and reproduce exponential polynomials~%
    \citep*{DL1995,CD1996,DL2002,CCGP2016a}.
    
    \item \glsadd{Multiple subdivision}%
    \emph{Multiple subdivision} schemes
    generalize non-stationary schemes to some extent by allowing in each step of the process
    a different dilation. %, and thus a different topology of the underlying mesh.
    Multiple subdivision schemes are building blocks for processing of images with 
    anisotropic directional features
    and for multigrid methods for solving anisotropic PDEs, 
    see e.g.~\citep*{KS2009,Sauer2012,CGRS2015} and~\citep*{CDRT2017}, respectively.
    
    \item \emph{Hermite subdivision} schemes refine not only point positions, but also derivatives or normals~%
    \citep*{Merr1992,JSch2002}.
    
    \item \emph{Non-linear subdivision} schemes use non-linear rules to generate new vertices,
    which can help to eliminate artefacts, preserve certain shape properties and 
    allows to generate smooth curves on manifolds~%
    \citep*{DW2005,Grohs2008}.
    
    \item \emph{Set-valued subdivision} schemes refine general subsets of $\RR^n$~
    \citep*{DK2011}.
\end{itemize}

\glsadd{Matrix approach}\glsadd{Operator approach}%
One of the most important properties of curves or surfaces generated by subdivision is their smoothness.  
In the case of level independent or dependent subdivision weights (stationary and non-stationary schemes)
this property is well understood and usually characterized either using the 
matrix or operator approach,
see e.g.~\citep*{CDM1991, DL2002, CCS2005}
and
\citep*{DL1992a, CH1994, HJ1998, CJR2002, Han2003, CHM2004, CP2017}, respectively.


%The subdivision shapes are characterized in terms of algebraic properties of 
%subdivision symbols~
%\citep*{CDM1991, Jia1998, JP2001, MS2004, CCR2014}. 


The matrix approach studies the spectral properties of
finite sets of square matrices derived from the subdivision masks.
%The connection between stationary (level independent) subdivision and 
%joint spectral radius techniques was established in~\citep*{DL1992a, DL1992b}. 
The essential ingredients are the so-called
transition matrices whose entries depend on the subdivision weights and whose structure is inherited from the
dilation matrix, i.e.\ the underlying mesh. 
The main challenge of adapting the matrix approach to the case of multiple subdivision, 
and thus level dependent dilation matrices, is in combining the properties of weights and dilations into an 
appropriate structure of the corresponding transition matrices. 
Recent advances by \citet*{GP2013, GP2016} in the exact computation of the joint spectral radius 
of compact sets of square matrices 
provide efficient methods for checking both Hölder and Sobolev regularity of subdivision surfaces
using such transition matrices.

\enlargethispage{1\baselineskip}
The operator approach studies
the contractivity of the corresponding difference subdivision sche\-mes.
The study of the properties of multiple subdivision is at its very beginning. 
The convergence analysis of multiple subdivision 
in terms of the restricted spectral radius is given in~\citep*{Sauer2012}.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EOF %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                                                    